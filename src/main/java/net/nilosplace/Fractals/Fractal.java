package net.nilosplace.Fractals;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;
import java.awt.image.BufferStrategy;

import javax.swing.JFrame;

public class Fractal extends JFrame implements MouseListener, MouseMotionListener {

	private static final long serialVersionUID = -8420765764692298203L;
	private int w = 1600;
	private int h = 1200;
	
	private double halfw = ((double)w / (double)2);
	private double halfh = ((double)h / (double)2);

	private Point pressed;
	private Point.Double trans;
	
//	private Point.Double wpmin = new Point2D.Double(-2.5, -1);
//	private Point.Double wpmax = new Point2D.Double(1, 1);
//	
//	private Point.Double vpmin = new Point2D.Double(0, 0);
//	private Point.Double vpmax = new Point2D.Double(w, h);
	
//	private double sx = (vpmax.x - vpmin.x) / (wpmax.x - wpmin.x);
//	private double sy = (vpmax.y - vpmin.y) / (wpmax.y - wpmin.y);
//	
//	private double tx = ((wpmax.x * vpmin.x) - (wpmin.x * vpmax.x)) / (wpmax.x - wpmin.x);
//	private double ty = ((wpmax.y * vpmin.y) - (wpmin.y * vpmax.y)) / (wpmax.y - wpmin.y);
	
	// Favorite spot
	private double sx = 559912.9782739371;
	private double sy = 734885.7839845422;
	private double tx = 782546.677600376;
	private double ty = 14111.51836547885;
	
	private Color[] colors = new Color[256*6];

	public static void main(String args[]) {
		new Fractal();
	}

	public Fractal() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//setUndecorated(true);
		setSize(w,h);
		addMouseListener(this);
		addMouseMotionListener(this);
		setVisible(true);

		createBufferStrategy(2);
		makeColors();
		printInfo();
		drawStuff();
	}

	private void makeColors() {
		/*
		
		0, 0, 0
		256, 0, 0
		256, 256, 0
		0, 256, 0
		0, 256, 256
		0, 0, 256
		0, 0, 0
		
		000000 Black
		FF0000 Red
		FFFF00 Yellow
		0FFFF0 Green
		00FFFF Cyan
		0000FF Blue
		000000 Black
		
		*/
		
		for(int i = 0; i < 256; i++) {
			colors[i + (256 * 0)] = new Color(i, 0, 0);
			colors[i + (256 * 1)] = new Color(255, i, 0);
			colors[i + (256 * 2)] = new Color(255-i, 255, 0);
			colors[i + (256 * 3)] = new Color(0, 255, i);
			colors[i + (256 * 4)] = new Color(0, 255-i, 255);
			colors[i + (256 * 5)] = new Color(0, 0, 255-i);
			
//			// index i
//			System.out.println(i + ",0.0");
//			// index i+(256 * 1)
//			System.out.println("255" + "," + i + ",0");
//			// index i+(256 * 2)
//			System.out.println((255 - i) + ",255,0");
//			// index i+(256 * 3)
//			System.out.println("0,255," + i);
//			// index i+(256 * 4)
//			System.out.println("0," + (255 - i) + ",255");
//			// index i+(256 * 5)
//			System.out.println("0,0," + (255 - i));
		}
		
	}

	private void drawStuff() {
		BufferStrategy bf = this.getBufferStrategy();
		Graphics g = null;
		try {
			g = bf.getDrawGraphics();

			// It is assumed that mySprite is created somewhere else.
			// This is just an example for passing off the Graphics object.
			setBackground(Color.BLACK);
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, w, h);
			
			for(int i = 0; i < w; i++) {
				for(int j = 0; j < h; j++) {
					Color c = computeFractal(i, j);
					g.setColor(c);
					g.fillRect(i, j, 1, 1);
				}
				//System.out.println("Line Finished");
			}
			//System.out.println("Paint Finished");

		} finally {
			// It is best to dispose() a Graphics object when done with it.
			g.dispose();
		}

		// Shows the contents of the backbuffer on the screen.
		bf.show();

		//Tell the System to do the Drawing now, otherwise it can take a few extra ms until 
		//Drawing is done which looks very jerky
		Toolkit.getDefaultToolkit().sync();
	}

	public Color computeFractal(int in_x, int in_y) {
		Point.Double p = tranform(new Point(in_x, in_y));
		
		double xc = 0;
		double yc = 0;
		double xt = 0;

		int iteration = 0;
		int max_iteration = 250;
		boolean lessThenFour = false;
		double xcs = 0;
		double ycs = 0;
		
		do {
			xcs = xc * xc;
			ycs = yc * yc;
			xt = (xcs - ycs) + p.x;
			yc = (yc * xc * 2) + p.y;
			xc = xt;
			lessThenFour = (xcs + ycs) <= 4;
		} while(lessThenFour && iteration++ < max_iteration);
		
		double mu = iteration + 1 - Math.log(Math.log(Math.abs(xcs + ycs))) / Math.log(2);
		//System.out.println("Mu: " + mu);

		
		if(lessThenFour) {
			return Color.BLACK;
		} else {
			return colors[(int)((mu / ((double)max_iteration + 1)) * colors.length)];
//			int r = (int)((mu / ((double)max_iteration + 1)) * (double)256);
//			int g = (256 - r);
//			return new Color(g,g,g);
		}
	}

	public void mouseClicked(MouseEvent e) {
//		double divx = 0;
//		double divy = 0;

		
		if(e.getButton() == 3) {
			System.out.println("zoom out");
			
			tx -= ((double)e.getX() - halfw);
			ty -= ((double)e.getY() - halfh);
			
			double x = (tx - halfw) / sx;
			sx /= 1.2;
			tx = ((x * sx) + halfw);
			
			double y = (ty - halfh) / sy;
			sy /= 1.2;
			ty = ((y * sy) + halfh);

		}
		if(e.getButton() == 1) {
			System.out.println("zoom in");

			tx -= (((double)e.getX() - halfw));
			ty -= (((double)e.getY() - halfh));
			
			double x = (tx - halfw) / sx;
			sx *= 1.2;
			tx = ((x * sx) + halfw);
			
			double y = (ty - halfh) / sy;
			sy *= 1.2;
			ty = ((y * sy) + halfh);

		}
		printInfo();	
		drawStuff();
	}
	public void mousePressed(MouseEvent e) {
		pressed = new Point(e.getX(), e.getY());
		trans = new Point.Double(tx, ty);
	}

	public void mouseDragged(MouseEvent e) {
		//tx = trans.x + ((((double)e.getX() - pressed.x) / sx) * sx);
		//ty = trans.y + ((((double)e.getY() - pressed.y) / sy) * sy);
		tx = trans.x + ((double)e.getX() - pressed.x);
		ty = trans.y + ((double)e.getY() - pressed.y);
		printInfo();
		drawStuff();
	}
	
	public Point2D.Double tranform(Point p) {
		return new Point2D.Double(((double)p.x - tx) / sx, ((double)p.y - ty) / sy);
	}
	
	public void printInfo() {
		System.out.println("Sx: " + sx + " Sy: " + sy);
		System.out.println("Tx: " + tx + " Ty: " + ty);
		System.out.println("Upper: " + tranform(new Point(0, 0)));
		System.out.println("Upper: " + tranform(new Point(w, h)));
	}

	public void mouseReleased(MouseEvent e) {}
	public void mouseMoved(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}

}